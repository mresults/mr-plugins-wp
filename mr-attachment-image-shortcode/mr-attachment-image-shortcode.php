<?php
/*
Plugin Name: MR Attachment Image Shortcode
Plugin URI: http://www.marketingresults.com.au
Description: Adds a simple shortcode to obtain an attachment image URI
Author: Shaun Johnston, Marketing Results <shaun@marketingresults.com.au>
Version: 1.0
Author URI: http://about.me/shaun.johnston
*/

function mr_attachment_image_shortcode() {
	$args = func_get_args();
	$atts = $args[0];
	
	if (!isset($atts['id'])) {
		return '';
	}
	
	$image = wp_get_attachment_image_src(
		$atts['id'], 
		(isset($atts['size'])) ? $atts['size'] : 'full', 
		(isset($atts['icon'])) ? $atts['icon'] : false
	);
	
	if (isset($atts['tag']) && $atts['tag'] == true) {
		return "<img src=\"{$image[0]}\" width=\{$image[1]}\" height=\"{$image[2]}\" />";
	}
	
	return $image[0];
	
}

add_shortcode('mr-attachment-image', 'mr_attachment_image_shortcode');

?>