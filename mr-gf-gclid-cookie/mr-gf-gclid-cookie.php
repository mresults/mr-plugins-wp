<?php
/*
Plugin Name: MR Gravity Forms GCLID
Plugin URI: http://www.marketingresults.com.au
Description: Drops GCLID values encountered in a URI into a cookie for use in Gravity Forms form prefills
Author: Shaun Johnston, Marketing Results <shaun@marketingresults.com.au>
Version: 1.0
Author URI: http://about.me/shaun.johnston
*/

// Captures a GCLID passed in a URI, and stores it in a cookie
function mr_gf_gclid_cookie_capture() {
	
	if (isset($_GET['gclid'])) {
		
		// Set a cookie containing the GCLID value, to expire in one month
		setcookie('mr-gf-gclid-cookie', $_GET['gclid'], strtotime('+1 month'));

	}
}

// Sifts a Gravity Forms form for fields set to dynamically prefill with a gclid placeholder
// and prefills qualifying fields with either a GCLID in the GET request, or a stored GCLID valud
function mr_gf_gclid_cookie_prefill($form) {
	
	// Iterate over the field list, using each as a reference that can be written back to
	foreach ($form['fields'] as &$field) {
		
		// Check for some qualifiers that determine this field should be modified
		if (
			isset($field['allowsPrepopulate']) &&
			$field['allowsPrepopulate'] &&
			isset($field['inputName']) &&
			$field['inputName'] == 'gclid'
		) {
			
			// Set the field default from either the GET request GCLID or a stored GCLID 
			$field['defaultValue'] = (isset($_GET['gclid'])) 
				? $_GET['gclid'] 
				: (isset($_COOKIE['mr-gf-gclid-cookie']))
					? $_COOKIE['mr-gf-gclid-cookie']
					: '';
		}
	}
	
	// Send the form back to render
	return $form;
}

// Capture the GCLID on initialisation
add_action('init', 'mr_gf_gclid_cookie_capture');

// Prefill any fields set to be dynamically set with the GCLID field on form render
add_action('gform_pre_render', 'mr_gf_gclid_cookie_prefill');