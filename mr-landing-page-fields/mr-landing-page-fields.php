<?php
/*
Plugin Name: MR Landing Page Fields
Plugin URI: http://www.marketingresults.com.au
Description: Adds functional CSS and Javascript fields to landing pages.  Requires the Advanced Custom Fields and Advanced Custom Fields Code Field plugins
Author: Shaun Johnston, Marketing Results <shaun@marketingresults.com.au>
Version: 1.01
Author URI: http://about.me/shaun.johnston
*/

include_once(ABSPATH . 'wp-admin/includes/plugin.php');

define('LP_FIELDS_URL', plugin_dir_url(__FILE__));

function mr_landing_page_fields_dependencies_active() {
	return (
		is_plugin_active('advanced-custom-fields/acf.php') &&
		is_plugin_active('advanced-custom-fields-code-area-field/acf-code_area.php')
	);
}

if (mr_landing_page_fields_dependencies_active()) {
	
//	include_once(plugins_url() . '/advanced-custom-fields/acf.php');
	
	register_field_group(array (
		'id' => 'acf_landing-page-customisation',
		'title' => 'Landing Page Customisation',
		'fields' => array (
			array (
				'key' => 'field_54f8e4814d66f',
				'label' => 'CSS',
				'name' => 'css',
				'type' => 'code_area',
				'instructions' => 'Add any CSS that is specific to this landing page here.	It will only be used on this landing page.',
				'language' => 'css',
				'theme' => 'xq-light',
			),
			array (
				'key' => 'field_54f8e5e14d672',
				'label' => 'Javascript',
				'name' => 'javascript',
				'type' => 'code_area',
				'instructions' => 'Add any Javscript that is specific to this landing page here.	It will only be used on this landing page.',
				'language' => 'javascript',
				'theme' => 'xq-light',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'fbtab',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));

	add_action('the_post', 'mr_landing_page_fields_css');
	add_action('the_post', 'mr_landing_page_fields_js');
	
}

function mr_landing_page_fields_css() {

	global $post;
	
	if (!(($post->post_type == 'page' || $post->post_type == 'fbtab') && mr_landing_page_fields_dependencies_active())) {
		return;
	}
		
	$css = do_shortcode(preg_replace("/<(\/)?style>/", '', get_field('css')));
		
	if ($css) {
		wp_enqueue_style('mr-landing-page-fields-css', LP_FIELDS_URL . '/css/mr-landing-page-fields.css');
		wp_add_inline_style('mr-landing-page-fields-css', $css);
	}

}

function mr_landing_page_fields_js() {
	
	global $post;
	
	if (!(($post->post_type == 'page' || $post->post_type == 'fbtab') && mr_landing_page_fields_dependencies_active())) {
		return;
	}
	
	$js = do_shortcode(preg_replace("/<(\/)?script>/", '', get_field('javascript')));
		
	if ($js) {
		wp_register_script('mr-landing-page-fields-js', LP_FIELDS_URL . '/js/mr-landing-page-fields.js');
		$translation = array(
			'javascript' => __($js, 'mr-landing-page-fields'),
		);
		wp_localize_script('mr-landing-page-fields-js', 'mr_landing_page_vars', $translation);
		wp_enqueue_script('mr-landing-page-fields-js');
	}
		
}

?>